import { storeId } from '../config'
import request from '../utils/request'
//获取首页数据接口
export function getHome() {
	return request.get('index/lists')
}

//获取好物优选商品列表
export function homeNewProduct(data) {
	return request.get('homeNewProduct/list', {...data, storeId:storeId})
}
//获取好物优选商品列表
export function homeRecommendProduct(data) {
	return request.get('homeRecommendProduct/list', {...data, storeId:storeId})
}
//获取好物优选商品列表
export function getBestList(data) {
	return request.get('goodsList', {...data, storeId:storeId})
}

//获取广告列表
export function getAdList(data) {
	return request.get('bannerList', {...data, orderCount: 1,storeId:storeId})
}

//购物车数量
export function getCartNum(data) {
	return request.get("shoppingcart/count", data)
}

// 商品分类
export function getCatrgory() {
	return request.get('queryAllThreeCategoryByStoreId', {storeId:storeId})
}

// 购物车列表
export function getCartList() {
	return request.get('shoppingcart/carts')
}

//购物车选中状态
export function changeCartSelset(data) {
	return request.post('shoppingcart/selected',  data)
}

// 购物车数量更改
export function changeGoodsCount(data) {
	return request.post("shoppingcart/cartnum", data)
}

// 删除商品
export function deleteGoods(data) {
	return request.post("shoppingcart/deletecart", data);
}

//商品详情
export function getGoodsDetail(data) {
	return request.get('queryGoodsDetail', data)
}
export function getStoreDetail() {
	return request.get('store/querystoreinfo', { storeId:storeId})
}


// 商品搜索
export function getGoodsSearch(data) {
	return request.get('goodsList', {...data, storeId:storeId})
}

//搜索页,热门搜索列表,和历史搜索列表
export function getSearchpage(data) {
	return request.get('goodsList',{...data, storeId:storeId})
}

// 清空历史搜索
export function clearSearch() {
	return request.get('goods/clearSearch')
}

//领券中心
export function getCouponList(data) {
	return request.get("getcouponlist", {...data, storeId:storeId});
}
//获取商品的优惠券
export function getGoodsCoupon(data) {
	return request.get("getcouponlist", {...data, storeId:storeId});
}



//文章分类
export function getCategoryList(data) {
    let {type} = data
    let url = type ? 'help/cateList' : 'article/columnList'
    delete data.type
    return request.get(url)
}

//文章列表
export function getArticleList(data) {
    let {type} = data
    let url = type ? 'help/helpList' : 'article/articleList'
    delete data.type
    return request.get(url, data)
}

// 文章详情
export function getArticleDetail(data) {
    let {type} = data
    let url = type ? 'help/desc' : 'articleDetail'
    delete data.type
    return request.get(url, { id: data.id })
}


//加入购物车
export function addCart(data) {
	return request.post('shoppingcart/addshoppingcart', data)
}
//订单支付结果
export function orderPreview(data) {
	return request.get("settlement", data);
}
//下单
export function orderBuy(data) {
	return request.post("submitorder", {...data, source: 4});
}

//订单支付结果
export function getOrderResult(data) {
	return request.get("orderdetail", data);
}

//下单获取优惠券
export function getOrderCoupon(data) {
	return request.post("coupon/orderCoupon", data);
}



// 获取评价列表
export function getOrderCommentList(data) {
	return request.get("querycustomerorders", data)
}

//评价列表
export function getCommentList(id) {
	return request.get("queryskucomments", {spuId:id,type:-1})
}
//评价列表
export function querycommentsummarize(id) {
	return request.get("querycommentsummarize", {spuId:id,type:-1})
}
//评价分类
export function getCommentCategory(id) {
	return request.get("goods_comment/category", {spuId: id,type:-1})
}
export function getNotice() {
    return request.get("notice/list", {storeId:storeId})
}
