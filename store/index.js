
import Store from "wxministore";
import {isAuthorize} from '../utils/wxutil'
let store = new Store({
    state: {
        isLogin: true,
        isAuth: true,
        user: {}
    },
    methods: {
       
    },
    pageLisener: {
        onShow(){
            isAuthorize().then(res => {
                store.setState({
                    isAuth: res
                })
            })
        }
    }
});

export default store