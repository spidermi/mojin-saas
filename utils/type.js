
// 订单状态 

export const orderType1 = {
    ORDER_ALL: '0', //全部
    ORDER_PAY: '1', //待付款
    ORDER_DELIVERY: '2',//待发货
    ORDER_DELIVERYED: '3',//待收货
    ORDER_FINISH: '4',//待收货
    ORDER_CLOSE: '5'//待收货
}
export const orderType = {
    ORDER_ALL: 'all', //全部
    ORDER_PAY: 'pay', //待付款
    ORDER_DELIVERY: 'delivery',//待收货
    ORDER_DELIVERYED: 'deliveryed',//待收货
    ORDER_FINISH: 'finish',//待收货
    ORDER_CLOSE: 'close'//待收货
}
// 分销订单状态
export const userOrderPromoteOrder = {
    ALL: "0",
    WAIT_RETURN: "1",
    HANDLED: '2',
    INVALED: '3'
}

//分页状态
export const loadingType = {
    LOADING: 'loading',
    FINISHED: 'finished',
    ERROR: 'error',
    EMPTY: 'empty'
}

// 选择类型
export const SelectType = {
    // 全选
    SELECT_ALL: 1,
    // 取消全选
    CANCEL_SELECT_ALL: 2,
    // 选中店铺
    SELECT_SHOP: 3,
    // 取消选中店铺
    CANCEL_SELECT_SHOP: 4,
    // 单选/取消 选中一样商品
    SELECT_SINGLE: 5
}

// 选择状态
export const SelectStatus = {
    NOT_SELECT: 0,
    SELECTED: 1
}

// 购物车状态
export const CarType = {
    EMPTY: 1,
    HAVE: 2
}

// 收藏状态
export const CollectType = {
    COLLECTION: true,
    CANCEL_COLLECTION: false
}

// 秒杀时间段状态
export const SeckillTimeType = {
    // 已结束
    HAVE_BEEN_END: 0,
    // 抢购中
    SECKILLING: 1,
    // 未开始
    HAVE_NOT_BEGIN: 2
}

// 售后状态
/**
     * 退款／退货状态
     * 1:退款申请 （用户发送退款请求）
     * 2:退款成功（商家同意退款）
     * 3:退款拒绝 （商家拒绝退款）
     * 4:退货申请 （用户发起退货请求）
     * 5:退货拒绝   （商家拒绝退货）
     * 6:退货审核通过等待用户填写物流（商家审核通过，等待用户寄回商品）
     * 7: 待收货  （用户已经寄回商品，等待商家收货确认）
     * 8：退货完成（商家收货并且同意退款给用户）
     * 9:退货失败（商家不同意退款）
     */
export const AfterSaleType1 = {
    // 售后申请 
    NORMAL: '1',
    // 退货申请
    HANDLING: '4',
    // 退款成功
    FINISH: '2'
}
// 售后状态
export const AfterSaleType = {
    // 售后申请
    all: 'all',
    refundApply: 'refundApply',
    // 处理中
    refundSucess: 'refundSucess',
     refundReApply: 'refundReApply',
    // 已处理
    refundReSucess: 'refundReSucess'
}

// 售后退款操作
export const refundOPtType = {
    // 仅退款
    ONLY_REFUND: 0,
    // 退货退款
    REFUNDS: 1
}

// 售后详情状态
export const AfterSaleDetailType = {
    // 申请退款
    APPLY_REFUND: 0,
    // 商家拒绝
    STORE_REFUSE: 1,
    // 商品待退货
    WAIT_GOODS_REFUNDS: 2,
    // 商家待收货
    STORE_WAIT_EXPRESS: 3,
    // 商家拒收货
    STORE_REFUSE_EXPRESS: 4,
    // 等待退款
    WAIT_REFUND: 5,
    // 退款成功
    REFUND_SUCCESS: 6
}

export const bargainCodeType = {
    ALL: "all",
    // 进行中
    RUNNING: "running",
    // 成功
    SUCCESS: "success",
    // 失败
    FAIL: "fail"
}


export const payWay = {
    wechat: 1,
    blance: 3
}

export const BargainStatus = { 
    NORMAL: 0,
    SUCCESS: 1,
    FAIL: 2
}

export const FansType = {
    ALL: 'all',
    FIRST: 'first',
    SECOND: 'second'
}

export const SortType = {
    NONE: '',
    ASC: 'asc',
    DESC: 'desc'
}