

// 轻提示
export function Tips(info = {}, navigateOpt) {
	let title = info.title || ''
	let icon = info.icon || 'none'
	let endtime = info.endtime || 2000
	if (title) wx.showToast({ title: title, icon: icon, duration: endtime })
	if (navigateOpt != undefined) {
		if (typeof navigateOpt == 'object') {
			let tab = navigateOpt.tab || 1, url = navigateOpt.url || '';
			switch (tab) {
				case 1:
					//跳转至 table
					setTimeout(function () {
						wx.switchTab({
							url: url
						})
					}, endtime);
					break;
				case 2:
					//跳转至非table页面
					setTimeout(function () {
						wx.navigateTo({
							url: url,
						})
					}, endtime);
					break;
				case 3:
					//返回上页面
					setTimeout(function () {
						wx.navigateBack({
							delta: parseInt(url),
						})
					}, endtime);
					break;
				case 4:
					//关闭当前所有页面跳转至非table页面
					setTimeout(function () {
						wx.reLaunch({
							url: url,
						})
					}, endtime);
					break;
				case 5:
					//关闭当前页面跳转至非table页面
					setTimeout(function () {
						wx.redirectTo({
							url: url,
						})
					}, endtime);
					break;
			}

		} else if (typeof navigateOpt == 'function') {
			setTimeout(function () {
				navigateOpt && navigateOpt();
			}, endtime);
		}
	}
}


export function getUrlParams(params, s1 = '&', s2 = '=') {
	let value = {};
	if (typeof params != 'string') return {}
	params = params.split(s1);
	for (let prop in params) {
		let item = params[prop].split(s2);
		value[item[0]] = item[1];
	}
	return value;
}


export const trottle = (func, time = 1000, context) => {
	let previous = new Date(0).getTime()
	return function (...args) {
		let now = new Date().getTime()
		if (now - previous > time) {
			func.apply(context, args)
			previous = now
		}
	}
}

export const debounce = (func, time = 1000, context) => {
	let timer = null
	return function (...args) {
		if (timer) {
			clearTimeout(timer)
		}
		console.log(this)
		timer = setTimeout(() => {
			func.apply(context, args)
		}, time)
	}
}

//跳转
export const navigateTo = (url, params) => {

	let p = '';
	if (params) {
		p = '?'
		for (let props in params) {
			p += `${props}=${params[props]}&`
		}
		p = p.slice(0,-1)
	}

	wx.navigateTo({
		url: url + p
	})
}