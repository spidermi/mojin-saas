
import event from './utils/events'
import store from "./store/index";
import { getCartNum } from './api/store'
App({
    onLaunch: function () {
        this.checkUpdate()
        event.on('GET_CART_NUM', () => {this.$getCartNum()})
        this.$getCartNum()
        this.getSystemInfo()
    },
    getSystemInfo() {
        wx.getSystemInfo({
            success: res => {
                let { statusBarHeight, platform } = res
                let navHeight
                if (platform == 'ios' || platform == 'devtools') {
                    navHeight = statusBarHeight + 44
                } else {
                    navHeight = statusBarHeight + 48
                }
                this.globalData.navHeight = navHeight;
                this.globalData.statusBarHeight = statusBarHeight
            },
            fail(err) {
                console.log(err);
            }
        })
    },
    $getCartNum() {
        getCartNum().then(res => {
            if(res.data == 0)return wx.removeTabBarBadge({index: 2})
            if(res.code == 200) {
                wx.setTabBarBadge({
                    index: 2,
                    text: String(res.data)
                  })
            }
        })
    },
    checkUpdate() {
        const updateManager = wx.getUpdateManager()

        updateManager.onCheckForUpdate(function (res) {
            // 请求完新版本信息的回调
            console.log(res.hasUpdate)
        })

        updateManager.onUpdateReady(function () {
            wx.showModal({
                title: '更新提示',
                content: '新版本已经准备好，是否重启应用？',
                success: function (res) {
                    if (res.confirm) {
                        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                        updateManager.applyUpdate()
                    }
                }
            })
        })

        updateManager.onUpdateFailed(function () {
            // 新版本下载失败
        })

    },
    globalData: {
        navHeight: 0,
        statusBarHeight: 0,
        platform: ''
    },
    store: store,
})