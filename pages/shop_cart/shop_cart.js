
import { getCartList, changeCartSelset, changeGoodsCount, deleteGoods } from '../../api/store'
import { patchPage } from 'miniprogrampatch'
import { Tips } from '../../utils/util';
import event from '../../utils/events'
Page = patchPage(Page);
Page({

    /**
     * 页面的初始数据
     */
    data: {
        //购物车状态 1为有 2为空 0则什么都不显示
        cartType: 0,
        isShow: false,
        cartLists: [],
        delPopup: false,
        totalPrice: ''
    },
    goodsDelete() {
        deleteGoods({
            cart_id: this.cartId
        }).then(res => {
            if(res.code == 200) {
                this.$getCartList()
            }
        })
    },
    changeDelPopup(e) {
        const {cartid} = e.currentTarget.dataset
        if(cartid) {
            this.cartId = cartid
        }
        this.setData({
            delPopup: !this.data.delPopup
        })
    },
    $getCartList() {
        getCartList().then(res => {
            wx.stopPullDownRefresh({
              success: (res) => {},
            })
            if(res.code == 200) {
var  cartLists1 = [];
                let cartType = 0
                console.log(res.data[0])
                if(res.data && res.data[0] && res.data[0].normalSkus.length > 0) {
                    cartType = 1
                    cartLists1=res.data[0].normalSkus
                }else {
                    cartType = 2
                }
                console.log(cartType)
                this.setData({
                    cartLists: cartLists1,
                    cartType,
                    isShow: true
                })
                this.sum();
                event.emit("GET_CART_NUM")
            }
        })
    },
    // 合计
    			sum() {
    				 var sumPrice = 0;
    				let len = this.data.cartLists.length;
    					for (let j = 0; j < this.data.cartLists.length; j++) {
    						// 当商品
    						if (this.data.cartLists[j].checked) {
    							sumPrice = sumPrice + this.data.cartLists[j].price * this.data.cartLists[j].num;
    						}
    					}

                this.setData({
                        totalPrice: sumPrice.toFixed(2),
                    })

    			},
    changOneSelect(e) {
        const {cartid, checked} = e.currentTarget.dataset

            this.data.cartLists.forEach((item) => {
				if(item.cartId==cartid){
				item.checked=!item.checked
				}
			})
			this.sum();
       // this.$changeCartSelset([cartid], !checked)
    },
    changeAllSelect() {
        const {isSelectedAll, cartLists} = this.data
        let cartid = cartLists.map(item => item.cartId)
        this.$changeCartSelset(cartid, !isSelectedAll)
    },
    $changeCartSelset(cartId, checked) {
        changeCartSelset({
            cart_id: cartId,
            checked
        }).then(res => {
            if(res.code == 200) {
                this.$getCartList()
            }
        })
    },
    countChange(e) {
        const {cartid} = e.currentTarget.dataset
        changeGoodsCount({
            id: cartid,
            num: e.detail
        }).then(res => {
            if(res.code == 200) {
                this.$getCartList()
            }
        })
    },
    goToConfirm() {
        let {cartLists} = this.data
        const data = {};
        const ids = [];
        cartLists.forEach((item) => {
            if(item.checked) {
                ids.push(item.cartId);
            }
        })

        if(data.length == 0) return Tips({title: '您还没有选择商品哦'})
        data.type = 'cart';
        				data.ids = ids.join(',');
        				data.isGroup = 0;
        				data.skuInfo = '';
        				data.groupId = 0;

        				this.sumPrice = 0;
        wx.navigateTo({
            url: `/pages/confirm_order/confirm_order?goods=${JSON.stringify(data)}&type=cart`
        })
    },
    goPage(e) {
        const {url} = e.currentTarget.dataset
        wx.navigateTo({
            url
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.$getCartList()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.$getCartList()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: async function () {
                    return {
                       title: '用户点击右上角分享',
                       path: "pages/index/index"
                   }
    },
    computed: {
        nullSelect: {
            require: ['cartLists'],
			fn({ cartLists }) {
			console.log(this.data.cartLists)
			console.log(cartLists)
                let index = cartLists.findIndex((item) => (item.checked))
                if(index == -1) {
                    return true
                }
                return false
			}
        },
        isSelectedAll: {
            require: ['cartLists'],
            fn({ cartLists }) {
                let index = cartLists.findIndex((item) => (!item.checked))
                if(index == -1) {
                    return 1
                }
                return 0
			}
        }
    }
})