
import { getOrderDetail, cancelOrder, delOrder, confirmOrder } from '../../api/user'
import { prepay } from '../../api/app'
import { Tips } from '../../utils/util'
import event from '../../utils/events'
import {wxpay} from '../../utils/wxutil'
import Toast from '../../components/weapp/toast/toast';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        orderDetail: {},
        isFirstLoading: true,
        type: 0,
        cancelTime: 0
    },
    onShowDialog() {
        let { showCancel } = this.data
        this.setData({
            showCancel: !showCancel
        })
    },
    async onConfirm() {
        const { type } = this.data
        let res = null
			switch(type) {
				case 0 : res = await cancelOrder(this.id) 
				break;
				case 1 : res = await delOrder(this.id) 
				break;
				case 2 : res = await confirmOrder(this.id) 
				break;
			}
        if (res.code == 200) {
            this.onShowDialog()
            if(type == 0 || type == 2) {
                Tips({ title: res.msg })
                this.$getOrderDetail()
            }else if(type == 1) {
                Tips({ title: res.msg }, { tab: 3, url: 1 });
            }
            event.emit('RESET_LIST')
        }
    },
    delOrder(e) {
        this.setData({
            type: 1
        })
        wx.nextTick(() => {
            this.onShowDialog()
        })

    },
    comfirmOrder(e) {
        this.setData({
            type: 2
        })
        wx.nextTick(() => {
            this.onShowDialog()
        })

    },
    cancelOrder(e) {
        this.setData({
            type: 0
        })
        wx.nextTick(() => {
            this.onShowDialog()
        })
    },
    payNow(e) {
        this.toast = Toast.loading({
            duration: 0, // 持续展示 toast
            forbidClick: true,
            message: '请稍等...',
          });
        prepay({
           type: 1,
                                orderCode:this.data.orderDetail.orderCode
        }).then(res => {
            if (res.code == 200) {
             var r = res.data
                        if(r.flag==-5){
                        Tips({ title: '微信生成订单出错' })

                        					return false;
                        				}
                        				if(r.flag==-3){
                        				Tips({ title: '没有待支付订单' })
                        					return false;
                        				}
                        				if(r.flag==-1){
                                                    				Tips({ title: '用户不存在' })
                                                    					return false;
                                                    				}
                        				if(r.flag==-7){
                                                    				Tips({ title: '没有设置网站地址' })
                                                    					return false;
                                                    				}

                        				if(r.flag==-8){
                        				Tips({ title: '参数错误' })
                        					return false;
                        				}
                Toast.clear()
                let args = res.data.data
                wxpay(args).then(()=> {
                    Tips({ title: '支付成功' })
                    this.$getOrderDetail()
                    event.emit('RESET_LIST')
                }).catch(() => {
                    
                })
            }
        })
    },
    $getOrderDetail() {
        getOrderDetail(this.id).then(res => {
            if(res.code == 200) {
                const cancelTime = new Date(res.data.closeTime).getTime() - Date.now()
                console.log(cancelTime)
                this.setData({
                    orderDetail: res.data,
                    cancelTime: cancelTime > 0 ? cancelTime : 0
                })
                wx.nextTick(() => {
                    this.setData({
                        isFirstLoading: false
                    })
                })
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.id = this.id ? this.id : options.id
        this.$getOrderDetail()
    },
    goPage(e) {
        let { url } = e.currentTarget.dataset
        wx.navigateTo({
            url
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})