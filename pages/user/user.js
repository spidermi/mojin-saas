
import { getUser ,getOrderCount} from '../../api/user'
import { showLoginDialog } from '../../utils/wxutil'
import event from '../../utils/events'
import { getMenu } from '../../api/app'
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		showNav: false,
		navH: 0,
		navBg: 'transparent',
		navC: '#fff',
		userInfo: {},
		myOptList: [{"name":"我的钱包","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_user_wallet.png","link":"\/pages\/user_wallet\/user_wallet","is_tab":0,"link_type":1},
		{"name":"我的分销","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_user_wallet.png","link":"\/pages\/user_fenxiao\/user_fenxiao","is_tab":0,"link_type":1},{"name":"我的优惠券","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_user_coupon.png","link":"\/pages\/user_coupon\/user_coupon","is_tab":0,"link_type":1},{"name":"等级服务","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_user_vip.png","link":"\/pages\/user_vip\/user_vip","is_tab":0,"link_type":1},{"name":"我的收藏","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_user_collection.png","link":"\/pages\/user_collection\/user_collection","is_tab":0,"link_type":1},{"name":"收货地址","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_user_address.png","link":"\/pages\/user_address\/user_address","is_tab":0,"link_type":1},{"name":"帮助中心","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_news_center.png","link":"\/pages\/news_list\/news_list?type=1","is_tab":0,"link_type":1},{"name":"联系客服","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/centre_contact_offical.png","link":"\/pages\/contact_offical\/contact_offical","is_tab":0,"link_type":1}]
	},
	goLogin() {
		let { $state } = this.data
		if($state.isLogin) {
			wx.navigateTo({
				url: '/pages/user_profile/user_profile',
			})
			return
		}
		wx.navigateTo({
			url: '/pages/login/login'
		})
	},
	goPage(e) {
		let { url } = e.currentTarget.dataset
		let {  $state } = this.data
		if (!$state.isLogin) {
			showLoginDialog()
			return
		}
		wx.navigateTo({
			url
		})
	},
	navigateTo(e) {
		const {item: {is_tab, link, link_type}} = e.currentTarget.dataset
        let { $state } = this.data
				if(!$state.isLogin) return showLoginDialog()
				console.log(e)
		switch(link_type) {
			case 1:
					// 本地跳转
					if(is_tab) {
							wx.switchTab({
								url: link,
							})
							return
					}
					wx.navigateTo({
							url: link,
					})
					break;
			case 2:
					// webview
					wx.navigateTo({
						url: "/pages/webview/webview?url=" + link,
					})
					break;
			case 3:
					// tabbar
					
	}
	},
	$getUserInfo() {
		getUser().then(res => {
			wx.stopPullDownRefresh({
				success: (res) => {
				
				},
			})
			if(res.code == 200) {
				this.setData({
					userInfo: res.data,
				})
			}else {
				this.setData({
					userInfo: {
						user_money: 0,
						user_integral: 0,
						coupon: 0
					}
				})
			}
		})
		getOrderCount().then(res => {
        			wx.stopPullDownRefresh({
        				success: (res) => {

        				},
        			})
        			if(res.code == 200) {
        				this.setData({
        				    orderInfo: res.data,
        				})
        			}else {
        				this.setData({
        					orderInfo: {
        						user_money: 0,
        						user_integral: 0,
        						coupon: 0
        					}
        				})
        			}
        		})

	},
	observeHeader() {
		this.observeLine = wx.createIntersectionObserver()
		this.observeLine.relativeToViewport({ top: 0 }).observe('.header-line', (res) => {
			if (res.intersectionRatio == 0) {
				wx.setNavigationBarColor({
					frontColor: "#000000",
					backgroundColor: '#fff'
				})
				this.setData({
					navBg: '#fff',
					navC: '#000'
				})
			} else {
				wx.setNavigationBarColor({
					frontColor: "#ffffff",
					backgroundColor: '#000'
				})
				this.setData({
					navBg: 'transparent',
					navC: '#fff'
				})
			}
		})
	},

	onCopy(e) {
		wx.setClipboardData({
			data: this.data.userInfo.id
		})
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.setData({
			navH: app.globalData.navHeight,
			statusBarH: app.globalData.statusBarHeight
		})
		if (options && options.recommondCode) {
        			wx.setStorageSync('recommondCode', options.recommondCode)
        		}
		//this.$getMenu()
		wx.nextTick(() => {
			this.observeHeader()
		});
	},
	$getMenu() {
		getMenu({type: 2}).then((res) => {
			if(res.code == 200) {
				this.setData({
					myOptList: res.data
				})
			}
		})
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow() {
		event.emit('GET_CART_NUM')
		this.$getUserInfo()
	},
	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {
		this.observeLine.disconnect()
	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
	
		this.$getUserInfo()
	//	this.$getMenu()
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

})