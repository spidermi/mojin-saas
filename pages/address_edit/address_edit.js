
import { editAddress, getOneAddress, hasRegionCode, addAddress } from '../../api/user'
import { Tips } from '../../utils/util'
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		addressObj: {
			name: '',
			mobile: '',
			province: '',
			city: '',
			district: '',
			detailAddress: '',
			isDefault: 0,
		},
		region: '',
		addressId: '',
	},
	formSubmit(e) {

		let { value } = e.detail;
		let {  addressObj: { provinceId, cityId, countryId, isDefault, detailAddress}, addressId } = this.data

		value.detailAddress = detailAddress
		if (!value.name) return Tips({ title: '请填写收货人姓名' });
		if (!value.mobile) return Tips({ title: '请填写手机号码' });
		if (!value.region) return Tips({ title: '请选择省、市、区' });
		if (!value.detailAddress) return Tips({ title: '请填写小区、街道、门牌号等信息' });
		value.provinceId = parseInt(provinceId)
		value.cityId = parseInt(cityId)
		value.countryId = parseInt(countryId)
		value.isDefault = isDefault
		value.address = this.data.region
		value.id = addressId
		delete value.region
		if(addressId) {
			editAddress(value).then(res => {
				if (res.code == 200) {
					Tips({ title: res.msg }, { tab: 3, url: 1 });
				}
			}).catch(err => {
				return Tips({ title: err });
			})
		}else {
			addAddress(value).then(res => {
				if (res.code == 200) {
					Tips({ title: res.msg }, { tab: 3, url: 1 });
				}
			}).catch(err => {
				return Tips({ title: err });
			})
		}
		
	},

	regionChange(e) {
		let { code, value } = e.detail
		let region = value.reduce((pre, cur) => {
			return pre + cur + ' '
		}, '')

		this.data.addressObj.provinceId = code[0]
		this.data.addressObj.cityId = code[1]
		this.data.addressObj.countryId = code[2]
		this.setData({
			region
		})
	},

	ChangeIsDefault: function (e) {
		if(this.data.addressObj.isDefault == 0) {
			this.setData({ 'addressObj.isDefault': 1 });
		}
		else {
			this.setData({ 'addressObj.isDefault': 0 });
		}
	},

	textareaChange: function (e) {
		this.setData({'addressObj.detailAddress': e.detail})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.setData({
			addressId: parseInt(options.id) 
		})
		if (options.id) {
			wx.setNavigationBarTitle({ title: '编辑地址' })
			this.$getOneAddress()
		} else {
			wx.setNavigationBarTitle({ title: '添加地址' })
			this.$getWxAddress()
		}
	},
	$getOneAddress() {
		getOneAddress(this.data.addressId).then(res => {
			if(res.code == 200) {
				let { address } = res.data
				this.setData({
					addressObj: res.data,
					region: `${address}`
				})
			}
		})
	},
	$getWxAddress() {
		let wxAddress = wx.getStorageSync('wxAddress')
		if (!wxAddress) return
		let { userName: name,
			telNumber: mobile,
			provinceName: province,
			cityName: city,
			countyName: district,
			detailInfo: address } = JSON.parse(wxAddress)
		hasRegionCode({
			province,
			city,
			district
		}).then(res => {
		console.log(res)
			if(res.code == 200) {
				if(res.data.province) {
					this.setData({
						region: `${province} ${city} ${district}`,
						'addressObj.name': name,
						'addressObj.mobile': mobile,
						'addressObj.detailAddress': address,
					})
					this.data.addressObj.provinceId = res.data.province
					this.data.addressObj.cityId = res.data.city
					this.data.addressObj.countryId = res.data.district
				}
			}
		})
		
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {
		wx.removeStorageSync('wxAddress')
	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: async function () {
             let userInfo = wx.getStorageSync("userInfo")
            if(userInfo) {
                return {

                    path: "pages/index/index?recommondCode="+userInfo.selfRecommendCode
                }
            } else {
                return {
                    path: "pages/index/index"
                }
            }
        }
})