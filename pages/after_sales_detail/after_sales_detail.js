
import {afterSaleDetail, cancelApply} from "../../api/user"
import { Tips, trottle } from "../../utils/util";
import event from "../../utils/events"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goods: {},
    reason: [],
    lists: {},
    copyContent: "",
    confirmDialog: false
  },

  onCopy() {
    let {lists, copyContent} = this.data;
    let {address} = lists.shop;
    copyContent = address
		wx.setClipboardData({
			data: copyContent
		})
	},

  goRefund(e) {
    let {lists} = this.data
    wx.navigateTo({
      url: '/pages/apply_refund/apply_refund?order_id=' + this.orderId + '&afterSaleId=' + this.afterSaleId + '&item_id=' + lists.order_goods.item_id,
    })
  },

  showDialog() {
    this.setData({
      confirmDialog: true
    })
  },

  hideDialog() {
    this.setData({
      confirmDialog: false,
    })
  },

  confirmCancel() {

  },

  $cancelApply() {
    cancelApply({id: this.afterSaleId}).then(res => {
      if(res.code == 200) {
        Tips({title: res.msg}, {tab: 5, url: '/pages/post_sale/post_sale'});
        event.emit('RESET_LIST');
       
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let {afterSaleId, order_id} = options;
    this.afterSaleId = afterSaleId
    this.orderId = order_id
  },

  $afterSaleDetail() {
    afterSaleDetail({id: this.afterSaleId}).then(res => {
      if(res.code == 200) {
        this.setData({
          lists: res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.$afterSaleDetail()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
 onShareAppMessage: async function () {
          let userInfo = wx.getStorageSync("userInfo")
         if(userInfo) {
             return {

                 path: "pages/index/index?recommondCode="+userInfo.selfRecommendCode
             }
         } else {
             return {
                 path: "pages/index/index"
             }
         }
     }
})