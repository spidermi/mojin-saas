
import { orderType,orderType1 } from '../../utils/type'
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		active: orderType.ORDER_ALL,
		order: [
			{
				name: '全部',
				status: orderType.ORDER_ALL,
				isShow: false
			},
			{
				name: '待付款',
				status: orderType.ORDER_PAY,
				isShow: false
			},
			{
				name: '待发货',
				status: orderType.ORDER_DELIVERY,
				isShow: false
			},
			{
				name: '待收货',
				status: orderType.ORDER_DELIVERYED,
				isShow: false
			},
			{
				name: '已关闭',
				status: orderType.ORDER_CLOSE,
				isShow: false
			}
		],

	},
	onChange(e) {
		const {name} = e.detail
		this.changeShow(name)
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		console.log(options)
		let status = options.status || orderType.ORDER_ALL
		this.init(status)
		
	},
	init(status) {
		this.changeShow(status)
	},
	changeShow(status) {
		const {order} = this.data
		let index = order.findIndex(item => {
			return item.status == status
		});
		if(index != -1) {
			this.setData({
				[`order[${index}].isShow`]: true,
				active: status
			})
		}
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		const { active } = this.data
		console.log(active)
		let myComponent = this.selectComponent('#' + active);
		console.log(myComponent)
		if (myComponent && myComponent.$getOrderList) {
			myComponent.$getOrderList()
		}
	},
})