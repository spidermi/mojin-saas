
import {getAfterSaleList, applyAfterSale, getGoodsInfo, inputExpressInfo, cancelApply, afterSaleDetail, applyAgain} from "../../api/user"
import {AfterSaleType} from "../../utils/type";
import event from '../../utils/events';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    afterSaleType: 0,
    afterSaleList: [],
    afterSale: [{
      name: '全部',
      type: AfterSaleType.all,
      isShow: true
    },{
           name: '退款申请',
           type: AfterSaleType.refundApply,
           isShow: true
         },{
                name: '退款成功',
                type: AfterSaleType.refundSucess,
                isShow: true
              },{
                               name: '退货申请',
                               type: AfterSaleType.refundReApply,
                               isShow: true
                             },{
                                              name: '退货成功',
                                              type: AfterSaleType.refundReSucess,
                                              isShow: true
                                            }]
  },

  onChange(e) {
    const {name} = e.detail
    this.changeShow(name);
  },

  changeShow(type) {
  console.log(type)
    const {afterSale} = this.data
		let index = afterSale.findIndex(item => {
			return item.type == type
    });
		if(index != -1) {
			this.setData({
				[`afterSale[${index}].isShow`]: true,
				active: type
			})
		}
  },

  goPage(e) {
    let {url} = e.currentTarget.dataset;
    wx.navigateTo({
      url: url,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.changeShow('all');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    event.remove("RESET_LIST")
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    const { active } = this.data
    let myComponent = this.selectComponent('#' + active);
    console.log(myComponent)
		if (myComponent.$getAfterSaleList) {
			myComponent.$getAfterSaleList()
		}
  },
})