
Page({

    /**
     * 页面的初始数据
     */
    data: {
        active: 0,
        coupons: [{
            title: '可使用',
            num: '',
            status: 1
        },{
            title: '已使用',
            num: '',
            status: 2
        },{
            title: '已过期',
            num: '',
            status: 3
        }]
    },
    onChangeNum(e) {
        const {index} = e.currentTarget.dataset
        this.setData({
            [`coupons[${index}].num`]: e.detail
        })

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
})