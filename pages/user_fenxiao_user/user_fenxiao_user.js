
import { subdistributioncustomer, cancelattention, } from "../../api/user"
import { CollectType, loadingType } from "../../utils/type"
import event from '../../utils/events'

Page({

    /**
     * 页面的初始数据
     */
    data: {
        page: 0,
        status: "loading",
        deleteSure: false,
        collectionList: [],
        collectionGoods: CollectType.COLLECTION,
    },

    deleteCancel: function (e) {
        this.setData({ deleteSure: false })
    },

    deleteConfirm: function (e) {
        this.id = e.currentTarget.dataset.id;
        this.setData({ deleteSure: true });
    },

    $getCollectGoods() {
        let { page, collectionList, status } = this.data
        if (status == loadingType.FINISHED) {
            return
        }
        subdistributioncustomer().then(res => {
            if (res.code == 200) {
                this.setData({
                    collectionList: res.data,
                    loadingStatus: loadingType.FINISHED
                })

            } else {
                this.setData({
                    status: loadingType.ERROR
                })
            }
        })
    },



    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        this.$getCollectGoods();
    },





   
    goToGoodsDetail: function (e) {
        let {id} = e.currentTarget.dataset;
        wx.navigateTo({
            url: '/pages/product/product?id='+id,
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        event.remove("REFLASH")
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
})