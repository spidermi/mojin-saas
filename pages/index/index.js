
import { getBestList, homeNewProduct ,getNotice,getCouponList,homeRecommendProduct,getStoreDetail} from '../../api/store'
import { getMenu, getCouponPopList, userShare } from '../../api/app'
import { Tips } from '../../utils/util'
import { showLoginDialog } from '../../utils/wxutil'
import event from '../../utils/events'

const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        currentSwiper: 0,
        colors : ["#FE41B7","#FA444D","#BD5AFF", "#FB831C", "#2DC1B9"],
        page:0,
        navH: 0,
        statusBarHeight: 0,
        searchH: 0,
        logo: '',
        status: 'loading',
        timeData: {},
        remainTime: "",
        // canReceiveCount
        navList1: [{"name":"领券中心","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/coupon_center.png","link":"\/pages\/user_getcoupon\/user_getcoupon","is_tab":0,"link_type":1},{"name":"会员中心","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/user_vip.png","link":"\/pages\/user_vip\/user_vip","is_tab":0,"link_type":1},{"name":"我的收藏","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/user_collection.png","link":"\/pages\/user_collection\/user_collection","is_tab":0,"link_type":1},{"name":"商城资讯","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/news_center.png","link":"\/pages\/news_list\/article_list","is_tab":0,"link_type":1},{"name":"帮助中心","image":"http:\/\/likeshopv2.yixiangonline.com\/images\/menu\/news_center1.png","link":"\/pages\/news_list\/news_list?type=1","is_tab":0,"link_type":1}],
        news: [],
        goodsList: [],
        seckill: {},
        hotGoods: [],
        newGoods: [],
        recomGoods: [],
        activityArea: [],
        showCoupop: false,
        couponPopList: []
    },
    swiperChange(e) {
        this.setData({
            currentSwiper: e.detail.current
        })
    },
    goPage(e) {
        const {item: {is_tab, link, link_type}} = e.currentTarget.dataset
        let { $state } = this.data
        if(!$state.isLogin) return showLoginDialog()
        switch(link_type) {
            case 1:
                // 本地跳转
                if(is_tab) {
                    wx.switchTab({
                      url: link,
                    })
                    return
                }
                wx.navigateTo({
                    url: link,
                })
                break;
            case 2:
                // webview
                wx.navigateTo({
                  url: "/pages/webview/webview?url=" + link,
                })
                break;
            case 3:
                // tabbar
                
        }
    },
    onChangeDate(e) {
        let timeData = {}
        for (let prop in e.detail) {
            if (prop !== 'milliseconds') timeData[prop] = ('0' + e.detail[prop]).slice(-2)
        }
        this.setData({
            timeData
        });
    },
    $getHome() {

        getNotice().then(res => {

                if (res.code == 200) {
                    this.setData({
                        news:res.data
                    })
                }
            })
             getCouponList({}).then(res => {
                        if (res.code == 200) {
                            this.setData({
                                coupon: res.data.list
                            })
                        }
                    })

                     getStoreDetail({}).then(res => {
                                if (res.code == 200) {
                                console.log(res)
                                    this.setData({
                                        logo: res.data.avatarPicture,
                                    })
                                }
                            })
        homeNewProduct({}).then(res => {
            if (res.code == 200) {
                this.setData({
                    newGoods: res.data,
                })
            }
        })
        homeRecommendProduct({}).then(res => {

                    if (res.code == 200) {
                        this.setData({
                            recomGoods: res.data,
                        })
                    }
                })
    },
    $getBestList() {
        let { page, goodsList, status } = this.data
        if (status == 'finished') return
        getBestList({

            pageNum: page
        }).then(res => {
            if (res.code == 200) {
                let { list, totalPages } = res.data
                goodsList.push(...list)
                this.setData({
                    goodsList,
                    page: ++page
                })
                wx.stopPullDownRefresh()
                wx.nextTick(() => {
                    if (totalPages<=page) {
                        this.setData({
                            status: 'finished'
                        })
                    }
                    if (goodsList.length <= 0) {
                        this.setData({
                            status: 'empty'
                        })
                    }
                })
            }
        })
    },

    handleArray(data, array = [], optNum = 5) {
        if(data.length <= optNum) {
            data.length > 0 && array.push(data)
            return array;
        }
        array.push(data.splice(0,optNum))
        return this.handleArray(data, array,optNum)
    },

    async $getMenu() {
       //   截取数组
              const arr = this.handleArray(this.data.navList1);
              this.setData({
                  navList: arr
              })
    },

    async $getCouponPopList() {
        const {code, data} = await getCouponList({});
        if(code == 200) {

            this.setData({
                couponPopList: data.list,
                showCoupop: data.list.length > 0 ? true : false
            })
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            navH: app.globalData.navHeight,
            statusBarHeight: app.globalData.statusBarHeight
        });
        if (options && options.recommondCode) {
			wx.setStorageSync('recommondCode', options.recommondCode)
		}
        this.$getMenu();
        this.$getBestList();
      
    },

    onShowCoupons() {
        this.setData({
            showCoupop: !this.data.showCoupop
        })
        wx.navigateTo({
                            url: '/pages/user_getcoupon/user_getcoupon',
                        })


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.$getHome()
        this.$getCouponPopList()
        event.emit("GET_CART_NUM")
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.data.page = 0
		this.data.goodsList = []
		this.setData({
			status: 'loading'
		})
		this.$getHome()
		this.$getBestList()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.$getBestList()
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: async function () {
         let userInfo = wx.getStorageSync("userInfo")
        if(userInfo) {
            return {

                path: "pages/index/index?recommondCode="+userInfo.selfRecommendCode
            }    
        } else {
            return {
                path: "pages/index/index"
            }
        }
    }
})