
import { getAccountLog,getWallet } from '../../api/user'
import { loadingType } from '../../utils/type'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 0,
    growthList: [],
    status: loadingType.LOADING,
    growth: ""
  },
  $getAccountLog() {
    let { page, growthList, status } = this.data
        if (status == loadingType.FINISHED) return
        getAccountLog({
            pageNum: page,
            source: 3
        }).then(res => {
            if (res.code == 200) {
                let { list, more, count } = res.data
                growthList.push(...list)
                this.setData({
                    growthList,
                    page: ++page,
                })
                wx.stopPullDownRefresh()
                wx.nextTick(() => {
                    if (totalPages<=page) {
                        this.setData({
                            status: loadingType.FINISHED
                        })
                    }
                    if (growthList.length <= 0) {
                        this.setData({
                            status: loadingType.EMPTY
                        })
                    }
                })
            }
        })
  },
  $getWallet() {
    getWallet().then(res => {
        if(res.code == 200) {
            this.setData({
                growth: res.data.user_growth
            })
        }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.$getWallet()
    this.$getAccountLog()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.$getAccountLog()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})