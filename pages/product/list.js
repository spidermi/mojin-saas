
import { getGoodsSearch, getSearchpage, clearSearch } from '../../api/store'
import { trottle, Tips } from '../../utils/util'
import { loadingType } from '../../utils/type'
import { getRect } from '../../utils/wxutil'
import { patchPage } from 'miniprogrampatch'
Page = patchPage(Page);
const app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		headerH: 0,
		keyword: '',
		status: loadingType.LOADING,
		page: 0,
		type: '2',
		goodsList: [],
		firstCateId:'',
		thirdCateId:'',
		secondCateId:'',
		priceSort: '1',
		saleSort: '',
		showHistory: false,
		hotList: [],
		historyList:[]
	},
	onChange(e)  {
		this.setData({
			keyword: e.detail
		})
	},
	changeType() {
		if (this.data.type === '1') {
			this.setData({
				type: '2'
			})
		} else {
			this.setData({
				type: '1'
			})
		}
	},
	$clearSearch() {
		clearSearch().then(res => {
			if(res.code == 200) {
			//	this.$getSearchpage()
			}
		})
	},
	onNormal() {
		this.setData({
			priceSort: '1',
			saleSort: '',
		})
		this.onSearch()
	},
	onPriceSort() {
		let {priceSort} = this.data
		this.setData({
			saleSort: '1',
		})
		if (priceSort == 2) {
			priceSort = 3
		} else {
			priceSort = 2
		}
		this.setData({
			priceSort
		})
		this.onSearch()
	},
	onSaleSort() {
		let {saleSort} = this.data
		this.setData({
			priceSort: '1',
		})
		if (saleSort == 'desc') {
			priceSort = 4
		} else {
			priceSort = 1
		}
		this.setData({
			priceSort
		})
		this.onSearch()
	},
	init(option) {
		let { id, name } = option
		if (option.secondCateId) {
          	this.secondCateId = option.secondCateId ? option.secondCateId : '';
          }
          if (option.firstCateId) {
                    	this.firstCateId = option.firstCateId ? option.firstCateId : '';
             }
		this.type = option.type
		console.log(this.type, option)
		getRect('.header-wrap').then(res => {
			this.setData({
				headerH: res.height
			})
		})
		if (true) {
			wx.setNavigationBarTitle({
				title: name
			})
			this.id = id
			this.$getGoodsSearch()
		} else {
			wx.setNavigationBarTitle({
				title: '搜索'
			})
			this.setData({
				showHistory: true
			})
		}
	},
	$getSearchpage() {
		getSearchpage().then(res => {
			if(res.code == 200) {
				let {history_lists, hot_lists} = res.data
				this.setData({
					hotList: hot_lists,
					historyList: history_lists
				})
			}
		})
	},
	onFocus() {
		this.setData({
			showHistory: true
		})
	},
	onClear() {
		if(this.id) {
			this.onSearch()
		}
	},
	onSearch() {
		this.setData({
			showHistory: false
		})
		this.data.page = 0
		this.setData({
			goodsList: [],
			status: loadingType.LOADING
		})
		wx.nextTick(() => {
			this.$getGoodsSearch()
		})
	},
	onChangeKeyword(e) {
		let {keyword} = e.currentTarget.dataset
		this.setData({
			keyword,
			showHistory: false
		})
		this.onSearch()
	},
	$getGoodsSearch() {
		let { page, goodsList, keyword, priceSort, saleSort, status } = this.data
		if (status == loadingType.FINISHED) return
		let params = {};
		if (keyword) {
                                      					params.name = keyword;
                                      			  }
        			  if (this.type == 1) {
        					params.thirdCateId = this.type == 1 ? this.id : '';
        			  }
        			   if (this.thirdCateId) {
                                                                          					params.thirdCateId = this.thirdCateId ? this.thirdCateId : '';
                                                                       }
        			   if (this.firstCateId) {
                                                    					params.firstCateId = this.firstCateId ? this.firstCateId : '';
                                                 }
        			  if (this.secondCateId) {
                              					params.secondCateId = this.secondCateId ? this.secondCateId : '';
                           }
        			   if (this.type == 0) {
                              					params.brandId = this.type == 1 ? this.id : '';
                              			  }
                              			  params.pageNum=page;
                              			    params.orderBys=priceSort;
		getGoodsSearch(params).then(res => {
			
			if (res.code == 200) {
				let { list, totalPages } = res.data
				goodsList.push(...list)
				this.setData({
					goodsList,
					page: ++page
				})
				wx.nextTick(() => {
					if (totalPages<=page) {
						this.setData({
							status: loadingType.FINISHED
						})
					}
					if (goodsList.length <= 0) {
						this.setData({
							status: loadingType.EMPTY
						})
					}
				})
				wx.nextTick(() => {
					wx.stopPullDownRefresh({
						success: (res) => {},
					})
				})
			}
		})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.onSort = trottle(this.onSort, 500, this)
		this.onNormal = trottle(this.onNormal, 500, this)
		this.onPriceSort = trottle(this.onPriceSort, 500, this)
		this.onSaleSort = trottle(this.onSaleSort, 500, this)
		this.init(options)
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
	
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.$getGoodsSearch()
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},
	computed: {

		// showDetail: {
		// 	require: ['keyword', 'cateId'],
		// 	fn({ keyword,  cateId}) {
		// 		if (keyword || cateId) {
		// 			return true
		// 		}
		// 		return false
		// 	}
		// }
	},
	watch: {
		// 监听属性
		keyword(value, old) {
			if(!value && !this.id ) {
				this.setData({
					showHistory: true
				})
			}
		},
		showHistory(value) {
			if(value) {
				this.$getSearchpage()
			}
		}
	}
})