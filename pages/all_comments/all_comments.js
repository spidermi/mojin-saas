
import { getCommentList, querycommentsummarize } from '../../api/store'
import { loadingType } from '../../utils/type'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        status: loadingType.LOADING,
		page: 0,
        type: '',
        commentList: [],
        categoryList: [],
        percent: '',
        isEmpty: true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: async function (options) {
        this.id = options.id
        await this.$getCommentCategory()
        this.$getCommentList()
    },
    onChangType(e) {
        let {id} = e.currentTarget.dataset
        let {type} = this.data
        if(id == type ) return
        this.setData({
            type: id,
            page: 0,
            commentList: [],
            status: loadingType.LOADING,
        })
        wx.nextTick(() => this.$getCommentList())
    },
    $getCommentCategory() {
        return new Promise(resolve => {
            querycommentsummarize(this.id).then(res => {
                let {code, data: {comment, percent }} = res
                if(code == 200) {
                    this.setData({
                        categoryList: res.data,
                        type: 0
                    })
                    wx.nextTick(() => resolve())
                }
            })
        })
    },
    $getCommentList() {
        let { page, status, commentList, type } = this.data
		if (status == loadingType.FINISHED) return
        getCommentList(this.id).then(res => {
			if (res.code == 200) {
				let { list, totalPages } = res.data
				commentList.push(...list)
				this.setData({
					commentList,
					page: ++page
				})
				wx.nextTick(() => {
					if (totalPages<=page) {
						this.setData({
							status: loadingType.FINISHED
						})
					}
					if (commentList.length <= 0) {
						this.setData({
							status: loadingType.EMPTY
						})
					}else {
                        this.setData({
                            isEmpty: false
                        })
                    }
				})
			}
		})
    },
    previewImage(e) {
        const {current, uri} = e.currentTarget.dataset
        let urls = uri
        wx.previewImage({
            current, // 当前显示图片的http链接
            urls // 需要预览的图片http链接列表
          })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
   onShareAppMessage: async function () {
            let userInfo = wx.getStorageSync("userInfo")
           if(userInfo) {
               return {

                   path: "pages/index/index?recommondCode="+userInfo.selfRecommendCode
               }
           } else {
               return {
                   path: "pages/index/index"
               }
           }
       }
})