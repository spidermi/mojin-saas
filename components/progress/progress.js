

Component({
  /**
   * 组件的属性列表
   */
  options: {
      multipleSlots: true, // 在组件定义时的选项中启用多slot支持
  },

  properties: {
      progressBarColor: {
          type: String,
          value: '#01B55B'
      },
      progressWidth: {
        type: Number,
        value: 90
      },
      progressHeight: {
        type: Number,
        value: 6
      },
      progressColor: {
        type: String,
        value: '#E5E5E5'
      },
      left: {
        type: Number,
        value: 0
      },
      barWidth: {
        type: Number,
        value: 30
      }
  },

  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () { 
      this.data.offset = this.data.progressWidth - this.data.barWidth;
      this.setData({offset: this.data.offset})
    },
    detached: function () { },
},

  /**
   * 组件的初始数据
   */
  data: {
      currentSwiper: 0,
      // 剩余滑行距离
      offset: 0,
      barLeft: 0
  },

  observers: {
    'left': function(value) {
      this.setData({barLeft: (value / 100) * this.data.offset});
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
      swiperChange: function (e) {
          this.setData({
              currentSwiper: e.detail.current
          })
      },
  }
})
