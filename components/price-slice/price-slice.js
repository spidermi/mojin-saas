
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        firstSize: {
            type: Number,
            value: 28
        },
        secondSize: {
            type: Number,
            value: 28
        },
        color: {
            type: String
        },
        weight: {
            type: Number,
            optionalTypes: [String],
            value: 400
        },
        price: {
            type: String,
            observer: function (val) {
                let { priceSlice } = this.data
                let price = parseFloat(val)
                price = String(price).split('.')
                priceSlice.first = price[0]
                priceSlice.second = price[1]
                this.setData({
                    priceSlice
                })
            }
        },
        showSubscript: {
            type: Boolean,
            value: false,
        },
        subscriptSize: {
            type: Number,
            value: 23
        },
        lineThrough: {
            type: Boolean,
            type: false
        }
    },
    /**
     * 组件的初始数据
     */
    data: {
        priceSlice: {}
    },

    /**
     * 组件的方法列表
     */
    methods: {

    },

})
