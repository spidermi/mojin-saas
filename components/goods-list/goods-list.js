
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		type: {
			type: String,
			value: 'double'
		},
		list: {
			type: Array,
			value: []
		},
		shopId: {
			type: Number
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {

	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		goPage(e) {
			let { url } = e.currentTarget.dataset
			wx.navigateTo({
				url: `/pages/${url}/${url}`,
			})
		},
	}
})
