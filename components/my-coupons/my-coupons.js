
import { getMyCoupon } from '../../api/user'
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        type: {
            type: Number,
            value: 0
        }
    },
    lifetimes: {
        attached: function () {
            this.$getMyCoupon()
        },
    },
    /**
     * 组件的初始数据
     */
    data: {
        couponList: [],
        showNull: false
    },

    /**
     * 组件的方法列表
     */
    methods: {
        $getMyCoupon() {
            const {type} = this.data
            getMyCoupon({status:type,pageNum:0,pageSize:100}).then(res => {
                if(res.code == 200) {
                    this.triggerEvent('getnum', res.data.list.length)
                    if(res.data.length <=0) {
                        this.setData({
                            showNull: true
                        })
                        return
                    }
                    this.setData({
                        couponList: res.data.list,
                    })
                }
            })
        }
    }
})
