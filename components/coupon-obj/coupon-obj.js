
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        list: {
            type: Array,
            value: [],
            observer: function (val) {
                this.setData({
                    coupons: val
                })
            }
        },
        type: {
            type: Number
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        coupons: []
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onSelect(e) {
            const { coupons, type } = this.data
            if(type == 1) return
            const { index } = e.currentTarget.dataset
            coupons.forEach((item, i) => {
                if(i != index) {
                    item.checked = 0
                }
            })
            coupons[index].checked = coupons[index].checked ? 0 : 1
            this.triggerEvent('select', coupons[index].checked ? coupons[index] : '' )
            this.setData({
                coupons
            })
            
        },

    }
})
