
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        status: {
            type: String,
            value: 'loading'
        },
        errorText: {
            type: String,
            value: '加载失败，点击重新加载'
        },
        loadingText: {
            type: String,
            value: '加载中...'
        },
        finishedText: {
            type: String,
            value: '我可是有底线的～'
        },
        slotFooter: {
            type: Boolean,
            value: false
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        onRefresh() {
            this.triggerEvent('refresh')
        }
    }
})
