
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        list: {
            type: Object,
            value: []
        },
        link: {
            type: Boolean,
            value: false
        },
        showRefund: {
            type: Boolean,
            value: false
        },
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    lifetimes: {
        attached() {
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        goToDetail(e) {

            let { id } = e.currentTarget.dataset
            wx.navigateTo({
                url: `/pages/product/product?id=${id}`
            })
        },
        goPage(e) {
            let {url} = e.currentTarget.dataset;
            wx.navigateTo({
              url: url,
            })
        }
    }
})
